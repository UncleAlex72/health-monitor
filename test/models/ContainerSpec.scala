package models

import java.net.URL
import java.time.Instant

import org.scalatest.{Matchers, WordSpec}
import play.api.libs.json._

import scala.io.Source

class ContainerSpec extends WordSpec with Matchers {

  val unhealthyContainer = Container(Some(State(Some(Health(
    "unhealthy",
    Seq(
      Log(Instant.parse("2019-02-08T18:05:55.228117178Z"), 1, """{"health": "oh no"}"""),
      Log(Instant.parse("2019-02-08T18:06:25.333992283Z"), 1, """{"health": "oh dear"}"""),
      Log(Instant.parse("2019-02-08T18:07:25.342312321Z"), 0, """{"health": "fine"}""")
    )
  )))))

  "An unhealthy container" should {
    "be readable" in {
      val containerJsonUrl: URL = classOf[ContainerSpec].getClassLoader.getResource("unhealthy.json")
      val json: String = Source.fromFile(containerJsonUrl.toURI).mkString
      Json.fromJson[Container](Json.parse(json)) match {
        case JsSuccess(actualContainer, _) =>
          actualContainer should ===(unhealthyContainer)
          1 should ===(1)
        case JsError(errors) =>
          fail(s"There were errors parsing the unhealthy json value: $errors")
      }
    }
    "correctly get the latest health diagnostics" in {
      unhealthyContainer.lastOutput should ===(Some("""{"health": "oh dear"}"""))
    }
  }
}
