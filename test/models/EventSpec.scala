package models

import java.net.URI

import cats._
import cats.data._
import cats.data.Validated._
import cats.syntax._
import cats.implicits._
import org.scalatest.{Matchers, WordSpec}
import play.api.libs.json._

import scala.io.Source

class EventSpec extends WordSpec with Matchers {

  type ValidationResult[A] = ValidatedNec[Int, A]

  "Reading a list of events" should {
    val eventsUri: URI = classOf[EventSpec].getClassLoader.getResource("events.jsons").toURI
    val eventJsons: Seq[String] = Source.fromURI(eventsUri).mkString.split('\n').toSeq
    val validatedEvents: ValidationResult[Seq[Event]] = {
      val empty: ValidationResult[Seq[Event]] = Seq.empty.validNec
      eventJsons.zipWithIndex.foldLeft(empty) { (previousValidation, eventJsonWithIndex) =>
        val (eventJson, idx) = eventJsonWithIndex
        val thisValidation: ValidationResult[Event] = Json.fromJson[Event](Json.parse(eventJson)) match {
          case JsSuccess(event, _) => event.validNec
          case JsError(_) => idx.invalidNec[Event]
        }
        (previousValidation, thisValidation).mapN(_ :+ _)
      }
    }
    "read all events successfully and correctly identify unhealthy events" in {
      validatedEvents match {
        case Valid(events) =>
          events.zipWithIndex.filter(_._1.isUnhealthy).map(_._2) should contain theSameElementsAs Seq(14)
        case Invalid(unreadableIndices) =>
          unreadableIndices.toList should contain theSameElementsAs Seq()
      }
    }
  }
}
