self.addEventListener('install', function(event) {
    // Perform install steps
    console.log("I am installing myself.")
});

self.addEventListener('push', function(e) {
    const unhealthyAlert = e.data.json();
    const id = unhealthyAlert._id;
    const containerName = unhealthyAlert.containerName;

    const iconUrl = '@routes.Assets.versioned("images/health-monitor.png")';
    const actions = [ { action: "view-action", title: "View" }];
    const options = {
        body: "",
        icon: iconUrl,
        vibrate: [100, 50, 100],
        data: {
            dateOfArrival: Date.now(),
            primaryKey: id,
        },
        actions: actions
    };
    e.waitUntil(
        self.registration.showNotification(containerName + " has failed", options)
    );
});

self.addEventListener('notificationclick', function(event) {
    const action = event.action;

    const urlBuilder = () => {
        const data = event.notification.data;
        const id  = data.primaryKey;
        switch (action || 'default') {
            case 'view-action':
                return `@routes.HomeController.single()?id=${id}`;
            default:
                return '@routes.HomeController.list()';
        }
    };
    const url = urlBuilder();
    const promiseChain = clients.openWindow(url);
    event.waitUntil(promiseChain)
});