package controllers

import cats.data.OptionT
import cats.instances.future._
import com.mohiva.play.silhouette.api.actions.SecuredRequest
import daos.UnhealthyAlertDao
import formatters.{InstantFormatter, MessageFormatter}
import play.api.mvc._
import uk.co.unclealex.mongodb.ID
import uk.co.unclealex.psm.{DefaultEnv, Silhouette, SilhouetteAuthorization, SilhouetteDao}

import scala.concurrent.{ExecutionContext, Future}

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
class HomeController(val silhouette: Silhouette,
                     val authorization: SilhouetteAuthorization,
                     val authInfoDao: SilhouetteDao,
                     val instantFormatter: InstantFormatter,
                     val messageFormatter: MessageFormatter,
                     val unhealthyAlertDao: UnhealthyAlertDao,
                     val browserPushPublicKey: String,
                     override val controllerComponents: ControllerComponents)
                              (implicit ec: ExecutionContext) extends AbstractController(controllerComponents) {

  def unauthorised: Action[AnyContent] = Action { implicit request =>
    Forbidden(views.html.goaway())
  }

  def name(implicit request: SecuredRequest[DefaultEnv, _]): String = request.identity.firstName.getOrElse("Stranger")

  def list(): Action[AnyContent] = silhouette.SecuredAction(authorization).async { implicit request  =>
    for {
      unhealthyAlerts <- unhealthyAlertDao.findAll(20)
    } yield {
      Ok(views.html.list(instantFormatter, name, unhealthyAlerts))
    }
  }

  def remove(id: ID): Action[AnyContent] = silhouette.SecuredAction(authorization).async { implicit request =>
    for {
      _ <- unhealthyAlertDao.removeById(id)
    } yield {
      Redirect(routes.HomeController.list())
    }
  }

  def single(): Action[AnyContent] = silhouette.SecuredAction(authorization).async { implicit request =>
    val fo: OptionT[Future, Result] = for {
      id <- OptionT.fromOption[Future](request.getQueryString("id").map(ID(_)))
      unhealthyAlert <- OptionT(unhealthyAlertDao.findById(id))
    } yield {
      Ok(views.html.single(instantFormatter, messageFormatter, name, unhealthyAlert))
    }
    fo.value.map(_.getOrElse(NotFound("")))
  }

  def javascript(): Action[AnyContent] = silhouette.SecuredAction(authorization) { implicit request =>
    Ok(views.js.javascript(browserPushPublicKey))
  }

}
