package controllers

import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import uk.co.unclealex.psm.{Silhouette, SilhouetteAuthorization, SilhouetteDao}

class PwaController(
                     val silhouette: Silhouette,
                     val authorization: SilhouetteAuthorization,
                     val authInfoDao: SilhouetteDao,
                     val assets: Assets,
                     override val controllerComponents: ControllerComponents) extends AbstractController(controllerComponents) {

  def siteWebManifest: Action[AnyContent] = Action { implicit request =>
    Ok(views.json.sitewebmanifest())
  }

  def browserConfig: Action[AnyContent] = Action { implicit request =>
    Ok(views.xml.browserconfig())
  }

  /**
    * Serve javascript for the service worker.
    * @return
    */
  def serviceWorker: Action[AnyContent] = silhouette.SecuredAction(authorization) { implicit request =>
    Ok(views.js.serviceworker())
  }

}
