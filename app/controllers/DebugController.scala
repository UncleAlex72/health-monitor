package controllers

import events.UnhealthyAlertQueue
import play.api.Application
import play.api.mvc._

trait DebugController extends BaseController {

  def alert(containerName: String): Action[String]

  def shutdown(): Action[AnyContent]
}

object DebugController {

  def apply(enabled: Boolean,
            application: () => Application,
            unhealthyAlertQueue: UnhealthyAlertQueue,
            controllerComponents: ControllerComponents): DebugController = {
    if (enabled) {
      new EnabledDebugController(application, unhealthyAlertQueue, controllerComponents)
    }
    else {
      new DisabledDebugController(controllerComponents)
    }
  }

  private class EnabledDebugController(application: () => Application,
                                       unhealthyAlertQueue: UnhealthyAlertQueue,
                                       override val controllerComponents: ControllerComponents)
    extends AbstractController(controllerComponents) with DebugController {
    override def alert(containerName: String): Action[String] = Action(parse.tolerantText(256)) { implicit request =>
      unhealthyAlertQueue.push(containerName, request.body)
      Ok("")
    }

    override def shutdown(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
      application().stop()
      Ok("")
    }
  }

  private class DisabledDebugController(override val controllerComponents: ControllerComponents)
    extends AbstractController(controllerComponents) with DebugController {
    override def alert(containerName: String): Action[String] =
      Action(parse.tolerantText(256)) { implicit request => NotFound("") }
    override def shutdown(): Action[AnyContent] =
      Action { implicit request => NotFound("") }
  }
}