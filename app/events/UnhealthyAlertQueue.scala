package events

trait UnhealthyAlertQueue {

  def push(name: String, payload: String): Unit

  def complete(): Unit
}
