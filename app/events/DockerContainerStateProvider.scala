package events

import models.Container

import scala.concurrent.Future

trait DockerContainerStateProvider {

  def apply(id: String): Future[Container]
}
