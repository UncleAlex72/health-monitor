package events.akka

import akka.NotUsed
import akka.stream.scaladsl.{RestartSource, Source}
import models.Event

import scala.concurrent.duration._

class ResilientEventSourceSupplierImpl(val eventSourceSupplier: EventSourceSupplier) extends ResilientEventSourceSupplier {

  override def apply(maybeMaxRetries: Option[Int]): Source[Event, NotUsed] = {
    RestartSource.withBackoff(
      minBackoff = 1.second,
      maxBackoff = 1.minute,
      randomFactor = 0.2,
      maxRestarts = maybeMaxRetries.getOrElse(-1))(() => eventSourceSupplier())
  }
}
