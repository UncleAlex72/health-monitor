package events.akka

import akka.NotUsed
import akka.stream.scaladsl.Flow
import models.UnhealthyAlert

trait PersistingUnhealthyAlertFlowSupplier {

  def apply(): Flow[UnhealthyAlert, UnhealthyAlert, NotUsed]
}
