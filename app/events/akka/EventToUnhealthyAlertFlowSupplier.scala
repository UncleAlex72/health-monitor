package events.akka

import akka.NotUsed
import akka.stream.scaladsl.Flow
import models.{Event, UnhealthyAlert}

trait EventToUnhealthyAlertFlowSupplier {

  def apply(): Flow[Event, UnhealthyAlert, NotUsed]
}
