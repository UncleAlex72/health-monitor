package events.akka

import akka.stream.scaladsl.Sink
import models.UnhealthyAlert
import uk.co.unclealex.push.BrowserPushService

class BrowserNotificationSinkSupplier(browserPushService: BrowserPushService) extends UnhealthyAlertSinkSupplier {

  override def apply(): Sink[UnhealthyAlert, _] = {
    Sink.foreachAsync(1) { unhealthyAlert =>
      browserPushService.notifyAll(unhealthyAlert)
    }
  }
}
