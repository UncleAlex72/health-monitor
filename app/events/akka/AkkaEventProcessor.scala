package events.akka

import akka.NotUsed
import akka.event.Logging
import akka.stream._
import akka.stream.scaladsl.{Broadcast, Keep, Sink, Source}
import cats.data.NonEmptyList
import events.{EventProcessor, Shutdown, UnhealthyAlertQueue}
import models.UnhealthyAlert

import scala.concurrent.{ExecutionContext, Future}

class AkkaEventProcessor(resilientEventSourceSupplier: ResilientEventSourceSupplier,
                         eventToUnhealthyAlertFlowSupplier: EventToUnhealthyAlertFlowSupplier,
                         unhealthyAlertSourceQueueSupplier: UnhealthyAlertSourceQueueSupplier,
                         persistingUnhealthyAlertFlowSupplier: PersistingUnhealthyAlertFlowSupplier,
                         unhealthyAlertSinkSuppliers: NonEmptyList[UnhealthyAlertSinkSupplier])
                        (implicit val materializer: Materializer, ec: ExecutionContext) extends EventProcessor {
  /**
    * Process all events.
    *
    * @return A shutdown hook that can be used to shut down this processor.
    */
  override def processAllEvents(): (Shutdown, UnhealthyAlertQueue) = {
    val firstSink: Sink[UnhealthyAlert, _] = unhealthyAlertSinkSuppliers.head()
    val otherSinks: List[Sink[UnhealthyAlert, _]] = unhealthyAlertSinkSuppliers.tail.map(_.apply())
    val sink: Sink[UnhealthyAlert, NotUsed] = {
      if (otherSinks.isEmpty) {
        firstSink.mapMaterializedValue(_ => NotUsed)
      }
      else {
        val secondSink: Sink[UnhealthyAlert, _] = otherSinks.head
        val proceedingSinks: List[Sink[UnhealthyAlert, _]] = otherSinks.tail
        Sink.combine(firstSink, secondSink, proceedingSinks :_*)(Broadcast(_))
      }
    }
    def loggingAttributes(onElement: Logging.LogLevel): Attributes = Attributes.logLevels(
      onElement = onElement,
      onFailure = Attributes.LogLevels.Error,
      onFinish = Attributes.LogLevels.Info)
    val killSwitch: SharedKillSwitch = KillSwitches.shared("storm-bringer")
    val eventBasedUnhealthyAlertSource: Source[UnhealthyAlert, NotUsed] = resilientEventSourceSupplier(None).
      via(killSwitch.flow).
      log("event").addAttributes(loggingAttributes(Attributes.LogLevels.Debug)).
      via(eventToUnhealthyAlertFlowSupplier())
    val queueBasedUnhealthyAlertSource: Source[UnhealthyAlert, UnhealthyAlertQueue] =
      unhealthyAlertSourceQueueSupplier().via(killSwitch.flow)

    val unhealthyAlertSource: Source[UnhealthyAlert, UnhealthyAlertQueue] =
      eventBasedUnhealthyAlertSource.mergeMat(queueBasedUnhealthyAlertSource)(Keep.right)

    val unhealthyAlertQueue: UnhealthyAlertQueue = unhealthyAlertSource.
        via(persistingUnhealthyAlertFlowSupplier()).
        log("alert").addAttributes(loggingAttributes(Attributes.LogLevels.Warning)).
        to(sink).run()
    val shutdown: Shutdown = () => Future.successful(killSwitch.shutdown())
    (shutdown, unhealthyAlertQueue)
  }
}
