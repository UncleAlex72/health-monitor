package events.akka

import akka.NotUsed
import akka.stream.scaladsl.Source
import models.Event

trait ResilientEventSourceSupplier {

  def apply(maybeMaxRetries: Option[Int]): Source[Event, NotUsed]
}
