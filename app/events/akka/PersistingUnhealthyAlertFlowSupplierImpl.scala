package events.akka
import akka.NotUsed
import akka.stream.scaladsl.Flow
import daos.UnhealthyAlertDao
import models.UnhealthyAlert

class PersistingUnhealthyAlertFlowSupplierImpl(unhealthyAlertDao: UnhealthyAlertDao) extends PersistingUnhealthyAlertFlowSupplier {

  override def apply(): Flow[UnhealthyAlert, UnhealthyAlert, NotUsed] = {
    Flow[UnhealthyAlert].mapAsync(1) { unhealthyAlert =>
      unhealthyAlertDao.store(unhealthyAlert)
    }
  }
}
