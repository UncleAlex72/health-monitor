package events.akka
import java.time.Clock

import akka.stream.OverflowStrategy
import akka.stream.scaladsl.Source
import events.UnhealthyAlertQueue
import models.UnhealthyAlert

class UnhealthyAlertSourceQueueSupplierImpl(enabled: Boolean, clock: Clock) extends UnhealthyAlertSourceQueueSupplier {
  override def apply(): Source[UnhealthyAlert, UnhealthyAlertQueue] = {
    if (enabled) {
      Source.queue[UnhealthyAlert](10, OverflowStrategy.backpressure).mapMaterializedValue(queue => new UnhealthyAlertQueue {
        override def push(name: String, payload: String): Unit = {
          val unhealthyAlert: UnhealthyAlert = UnhealthyAlert.synthetic(name, payload, clock)
          queue.offer(unhealthyAlert)
        }
        override def complete(): Unit = {
          queue.complete()
        }
      })
    }
    else {
      val unhealthyAlertQueue: UnhealthyAlertQueue = new UnhealthyAlertQueue {
        override def push(name: String, payload: String): Unit = {}

        override def complete(): Unit = {}
      }
      Source.empty.mapMaterializedValue(_ => unhealthyAlertQueue)
    }
  }
}
