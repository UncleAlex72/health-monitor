package events.akka

import akka.stream.scaladsl.Source
import events.UnhealthyAlertQueue
import models.UnhealthyAlert

trait UnhealthyAlertSourceQueueSupplier {

  def apply(): Source[UnhealthyAlert, UnhealthyAlertQueue]
}
