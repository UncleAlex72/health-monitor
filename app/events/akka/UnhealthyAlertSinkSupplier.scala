package events.akka

import akka.stream.scaladsl.Sink
import models.UnhealthyAlert

trait UnhealthyAlertSinkSupplier {

  def apply(): Sink[UnhealthyAlert, _]
}
