package events.akka

import java.io.EOFException

import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{Source, SourceQueueWithComplete}
import akka.{Done, NotUsed}
import com.typesafe.scalalogging.StrictLogging
import events.curl.DockerCommandBuilder
import models.Event
import models.Event._
import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}
import scala.sys.process._
import scala.util.Try

class DockerProcessEventSourceSupplier(dockerCommandBuilder: DockerCommandBuilder)
                                      (implicit val ec: ExecutionContext) extends EventSourceSupplier with StrictLogging {

  override def apply(): Source[Event, NotUsed] = {
    // Create a source that reads strings from a process and materializes this process so it can be killed.
    val curlProcessSource: Source[Either[Int, String], Process] = {
      val source: Source[Either[Int, String], SourceQueueWithComplete[Either[Int, String]]] =
        Source.queue[Either[Int, String]](10, OverflowStrategy.backpressure)
      source.mapMaterializedValue { queue =>
        val processLogger: ProcessLogger =
          ProcessLogger(
            line => queue.offer(Right(line)),
            line => logger.error(line))
        logger.info("Starting curl process to monitor for docker events.")
        val process: Process = dockerCommandBuilder("http://localhost/events").run(processLogger)
        // Offer an exit value when the process finishes so the source queue can be terminated.
        Future {
          val exitValue: Int = process.exitValue()
          queue.offer(Left(exitValue))
        }
        process
      }
    }
    // Make sure that the source errors if the curl process finishes. This means that docker has stopped.
    val errorOnExitCurlProcessSource: Source[String, Process] = curlProcessSource.map {
      case Right(line) => line
      case Left(exitValue) => throw new EOFException(s"external curl process finished unexpectedly with exit value $exitValue")
    }
    // Clean up the process by killing it when the source terminates.
    val cleanupCurlProcessSource: Source[String, Future[Done]] = errorOnExitCurlProcessSource.watchTermination() {
      (process, eventuallyDone) => {
        eventuallyDone.map { done =>
          logger.info("Destroying external curl process.")
          try {
            process.destroy()
          }
          catch {
            case ex: Throwable =>
              logger.warn("Destroying the curl process errored. Ignoring.", ex)
          }
          done
        }
      }
    }
    // Only look for events we can parse.
    val eventParser: String => Option[Event] = { line =>
      for {
        jsValue <- Try(Json.parse(line)).toOption
        event <- Json.fromJson[Event](jsValue).fold(_ => None, Some(_))
      } yield {
        event
      }
    }
    cleanupCurlProcessSource.map(eventParser).filter(_.isDefined).map(_.get).mapMaterializedValue(_ => NotUsed)
  }
}
