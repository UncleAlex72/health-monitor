package events.akka

import akka.NotUsed
import akka.stream.scaladsl.{Flow, Source}
import events.DockerContainerStateProvider
import models.{Event, UnhealthyAlert}

import scala.concurrent.ExecutionContext

class EventToUnhealthyAlertFlowSupplierImpl(containerStateProvider: DockerContainerStateProvider)
                                           (implicit ec: ExecutionContext) extends EventToUnhealthyAlertFlowSupplier {
  override def apply(): Flow[Event, UnhealthyAlert, NotUsed] = {
    // Introduce a builder for unhealthy alerts so that we can separate having to find the health check message
    type UnhealthyAlertBuilder = (String, String => UnhealthyAlert)
    val unhealthyAlertBuilderFlow: Flow[Event, UnhealthyAlertBuilder, NotUsed] = Flow.apply[Event].flatMapConcat { event =>
      val maybeUnhealthyAlert: Option[UnhealthyAlertBuilder] = {
        for {
          unhealthyEvent <- Some(event) if event.isUnhealthy
          containerName <- unhealthyEvent.actor.attributes.get("name")
        } yield {
          val containerId: String = unhealthyEvent.actor.id
          (containerId, (message: String) => UnhealthyAlert(None, containerId, containerName, message, unhealthyEvent.time))
        }
      }
      Source.fromIterator(() => maybeUnhealthyAlert.iterator)
    }
    val optionalUnhealthyAlertFlow: Flow[Event, Option[UnhealthyAlert], NotUsed] =
      unhealthyAlertBuilderFlow.mapAsync(2) { unhealthyAlertBuilder =>
        val (id, builder) = unhealthyAlertBuilder
        containerStateProvider(id).map { container =>
          container.lastOutput.map(output => builder(output.trim))
        }
    }
    optionalUnhealthyAlertFlow.filter(_.isDefined).map(_.get)
  }
}
