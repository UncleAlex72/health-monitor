package events.akka

import akka.NotUsed
import akka.stream.scaladsl.Source
import models.Event

trait EventSourceSupplier {

  def apply(): Source[Event, NotUsed]

}
