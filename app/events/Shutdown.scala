package events

/**
  * A trait that allows this processor to be shut down cleanly.
  */
trait Shutdown {

  /**
    * Cleanly shut down this processor.
    * @return
    */
  def shutdown(): Unit
}
