package events

/**
  * Retrieve events from docker and process them.
  */
trait EventProcessor {

  /**
    * Process all events.
    * @return A shutdown hook that can be used to shut down this processor.
    */
  def processAllEvents(): (Shutdown, UnhealthyAlertQueue)
}
