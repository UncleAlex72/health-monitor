package events.curl

/**
  * A trait that allows a command line process to access to the docker API.
  */
trait DockerCommandBuilder {

  def apply(url: String): Seq[String]
}
