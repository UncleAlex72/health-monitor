package events.curl

import java.io.IOException

import events.DockerContainerStateProvider
import models.Container
import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}
import scala.sys.process._


class CurlDockerContainerStateProvider(dockerCommandBuilder: DockerCommandBuilder)
                                      (implicit ec: ExecutionContext) extends DockerContainerStateProvider {

  override def apply(id: String): Future[Container] = Future {
    val response: String = dockerCommandBuilder(s"http://localhost/containers/$id/json").lineStream.mkString("\n")
    Json.fromJson[Container](Json.parse(response)) match {
      case JsSuccess(container, _) => container
      case JsError(errors) => throw new IOException(s"Parsing container response failed: $errors")
    }
  }
}
