package events.curl

class CurlDockerCommandBuilder extends DockerCommandBuilder {
  override def apply(url: String): Seq[String] = Seq("curl", "-s", "--unix-socket", "/var/run/docker.sock") :+ url
}
