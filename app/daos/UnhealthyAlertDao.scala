package daos

import java.time.Instant

import models.UnhealthyAlert
import uk.co.unclealex.mongodb.ID

import scala.concurrent.Future

trait UnhealthyAlertDao {

  def store(unhealthyAlert: UnhealthyAlert): Future[UnhealthyAlert]

  def findById(id: ID): Future[Option[UnhealthyAlert]]

  def findAllSince(since: Instant): Future[Seq[UnhealthyAlert]]

  def findAll(max: Int): Future[Seq[UnhealthyAlert]]

  def removeById(id: ID): Future[Int]
}