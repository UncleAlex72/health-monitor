package daos

import java.time.{Clock, Instant}

import models.UnhealthyAlert
import reactivemongo.api.indexes.{Index, IndexType}
import uk.co.unclealex.mongodb.Bson._
import uk.co.unclealex.mongodb.{DatabaseProvider, ID, MongoDbDao, Sort}

import scala.concurrent.{ExecutionContext, Future}

class MongoDbUnhealthyAlertDao(override val databaseProvider: DatabaseProvider, clock: Clock)(implicit ec: ExecutionContext)
  extends MongoDbDao[UnhealthyAlert](
    databaseProvider,
    clock,
    "unhealthy-alerts",
    MongoDbUnhealthyAlertDao.whenIndex) with UnhealthyAlertDao {

  override def defaultSort(): Option[Sort] = "when".desc

  override def findAllSince(since: Instant): Future[Seq[UnhealthyAlert]] = findWhere("when" >= since)

  override def findAll(max: Int): Future[Seq[UnhealthyAlert]] = super.findAll(max, None)

  override def removeById(id: ID): Future[Int] = super.removeById(id)
}

object MongoDbUnhealthyAlertDao {

  val whenIndex: Index = Index(
    key = Seq("when" -> IndexType.Descending),
    name = Some("whenIndex"))

}