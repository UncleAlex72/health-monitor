package loader

import java.time.Clock
import java.time.format.{DateTimeFormatter, FormatStyle}

import cats.data.NonEmptyList
import com.typesafe.scalalogging.StrictLogging
import controllers._
import daos.MongoDbUnhealthyAlertDao
import formatters.{InstantFormatter, MessageFormatter}
import events.akka._
import events.curl.{CurlDockerCommandBuilder, CurlDockerContainerStateProvider, DockerCommandBuilder}
import events.{DockerContainerStateProvider, EventProcessor}
import play.api.ApplicationLoader
import play.api.cache.ehcache.EhCacheComponents
import play.api.libs.ws.ahc.AhcWSComponents
import play.api.mvc.Call
import play.api.routing.Router
import router.Routes
import uk.co.unclealex.psm.ConfigLoaders._
import uk.co.unclealex.psm.{FixedValidUsersAuthorization, PlaySilhouetteMongoFromContext, SilhouetteAuthorization}
import uk.co.unclealex.push.PushComponents

import scala.concurrent.ExecutionContext
/**
  * Create all the components needed to run this application.
  * @param context The Play Context.
  */
class AppComponents(context: ApplicationLoader.Context)
  extends PlaySilhouetteMongoFromContext(context, "health")
    with AhcWSComponents
    with AssetsComponents
    with EhCacheComponents
    with PushComponents
    with StrictLogging {

  def redirectOnLogin: Call = Call("GET", "/health/")
  def redirectOnLogout: Call = Call("GET", "/health/")
  
  val mongoExecutionContext: ExecutionContext = actorSystem.dispatcher

  val clock: Clock = Clock.systemDefaultZone()

  val unhealthyAlertDao = new MongoDbUnhealthyAlertDao(databaseProvider, clock)

  val authorization: SilhouetteAuthorization = configuration.get[FixedValidUsersAuthorization]("silhouette")
  val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM, FormatStyle.MEDIUM)
  val instantFormatter: InstantFormatter = InstantFormatter.from(dateTimeFormatter, clock)
  val messageFormatter: MessageFormatter = MessageFormatter.chain(MessageFormatter.json)
  val homeController = new HomeController(
    silhouette,
    authorization,
    authInfoDao,
    instantFormatter,
    messageFormatter,
    unhealthyAlertDao,
    browserPushPublicKey,
    controllerComponents)

  val debug: Boolean = configuration.get[Boolean]("debug")

  val dockerCommandBuilder: DockerCommandBuilder = new CurlDockerCommandBuilder()
  val eventSourceSupplier: EventSourceSupplier = new DockerProcessEventSourceSupplier(dockerCommandBuilder)

  val resilientEventSourceSupplier: ResilientEventSourceSupplier = new ResilientEventSourceSupplierImpl(eventSourceSupplier)
  val containerStateProvider: DockerContainerStateProvider = new CurlDockerContainerStateProvider(dockerCommandBuilder)
  val eventToUnhealthyAlertFlowSupplier: EventToUnhealthyAlertFlowSupplier = new EventToUnhealthyAlertFlowSupplierImpl(containerStateProvider)
  val persistingUnhealthyAlertFlowSupplier: PersistingUnhealthyAlertFlowSupplier = new PersistingUnhealthyAlertFlowSupplierImpl(unhealthyAlertDao)
  val unhealthyAlertSourceQueueSupplier: UnhealthyAlertSourceQueueSupplier = new UnhealthyAlertSourceQueueSupplierImpl(debug, clock)
  val notificationSink: UnhealthyAlertSinkSupplier = new BrowserNotificationSinkSupplier(browserPushService)
  val eventProcessor: EventProcessor = new AkkaEventProcessor(
    resilientEventSourceSupplier,
    eventToUnhealthyAlertFlowSupplier,
    unhealthyAlertSourceQueueSupplier,
    persistingUnhealthyAlertFlowSupplier,
    NonEmptyList.of(notificationSink))
  val (shutdown, unhealthyAlertQueue) = eventProcessor.processAllEvents()

  applicationLifecycle.addStopHook { () =>
    logger.info("Shutting down")
    unhealthyAlertQueue.complete()
    actorSystem.terminate()
  }

  val pwaController = new PwaController(silhouette, authorization, authInfoDao, assets, controllerComponents)
  val debugController = DebugController(debug, () => application, unhealthyAlertQueue, controllerComponents)
  override def router: Router = new Routes(
    httpErrorHandler,
    socialAuthController,
    pushController,
    pwaController,
    assets,
    secureMetricsController,
    healthCheckController,
    homeController,
    debugController
  )
}
