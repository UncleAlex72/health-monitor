package formatters

import play.api.libs.json.Json

import scala.util.Try

trait MessageFormatter {

  def format(message: String): String
}

object MessageFormatter {

  def chain(messageFormatters: MessageFormatter*): MessageFormatter = {
    message: String => {
      val maybeFormattedMessage: Option[String] = messageFormatters.toStream.flatMap { messageFormatter =>
        Try(messageFormatter.format(message)).toOption
      }.headOption
      maybeFormattedMessage.getOrElse(message)
    }
  }

  val json: MessageFormatter = (message: String) => {
    Json.prettyPrint(Json.parse(message))
  }
}