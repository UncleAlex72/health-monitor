package formatters

import java.time.{Clock, Instant}
import java.time.format.DateTimeFormatter

trait InstantFormatter {

  def format(instant: Instant): String
}

object InstantFormatter {

  def from(dateTimeFormatter: DateTimeFormatter, clock: Clock): InstantFormatter = (instant: Instant) =>
    dateTimeFormatter.format(instant.atZone(clock.getZone))
}