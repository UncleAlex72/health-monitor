package models

import java.time.{Clock, Instant}

import play.api.libs.json.{Json, Writes}
import reactivemongo.bson.{BSONDocumentHandler, Macros}
import uk.co.unclealex.mongodb._
import uk.co.unclealex.mongodb.Bson._

case class UnhealthyAlert(_id: Option[ID], containerId: String, containerName: String, message: String, when: Instant)

object UnhealthyAlert {

  def synthetic(name: String, message: String, clock: Clock): UnhealthyAlert = {
    UnhealthyAlert(None, "synthetic", name, message, clock.instant())
  }

  implicit val unhealthyAlertIsPersistable: IsPersistable[UnhealthyAlert] = IsPersistable(
    getId = _._id,
    setId = id => _.copy(_id = Some(id))
  )

  implicit val unhealthyAlertHandler: BSONDocumentHandler[UnhealthyAlert] = Macros.handler[UnhealthyAlert]
  implicit val unhealthyAlertWrites: Writes[UnhealthyAlert] = Json.writes[UnhealthyAlert]

}
