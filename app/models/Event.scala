package models

import java.time.Instant
import play.api.libs.json._
import play.api.libs.functional.syntax._

/**
  * A model for a docker event.
  */
case class Event(eventType: String, action: String, actor: Actor, time: Instant) {

  val isUnhealthy: Boolean = action == "health_status: unhealthy"
}

case class Actor(id: String, attributes: Map[String, String])

object Event {

  implicit val actorReads: Reads[Actor] =
    (
      (JsPath \ "ID").read[String] and
      (JsPath \ "Attributes").read[Map[String, String]]
    )(Actor.apply _)

  implicit val eventReads: Reads[Event] =
    (
      (JsPath \ "Type").read[String] and
      (JsPath \ "Action").read[String] and
      (JsPath \ "Actor").read[Actor] and
      (JsPath \ "time").read[Long].map(Instant.ofEpochSecond)
    )(Event.apply _)
}
