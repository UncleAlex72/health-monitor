package models

import java.time.Instant
import play.api.libs.json._
import play.api.libs.functional.syntax._

case class Container(maybeState: Option[State]) {
  val isUnhealthy: Boolean = maybeState.exists(_.isUnhealthy)
  val lastOutput: Option[String] = {
    for {
      state <- maybeState
      health <- state.maybeHealth
      log <- health.logs.filter(_.exitCode == 1).sortBy(_.start).reverse.headOption
    } yield {
      log.output
    }
  }
}

case class State(maybeHealth: Option[Health]) {
  val isUnhealthy: Boolean = maybeHealth.exists(_.isUnhealthy)
}

case class Health(status: String, logs: Seq[Log]) {
  val isUnhealthy: Boolean = status == "unhealthy"
}

case class Log(start: Instant, exitCode: Int, output: String)

object Container {

  implicit val logReads: Reads[Log] =
    (
      (JsPath \ "Start").read[String].map(Instant.parse) and
      (JsPath \ "ExitCode").read[Int] and
      (JsPath \ "Output").read[String]
    )(Log.apply _)

  implicit val healthReads: Reads[Health] =
    (
      (JsPath \ "Status").read[String] and
      (JsPath \ "Log").readWithDefault[Seq[Log]](Seq.empty)
    )(Health.apply _)

  implicit val stateReads: Reads[State] =
    (JsPath \ "Health").readNullable[Health].map(State.apply)

  implicit val containerReads: Reads[Container] =
    (JsPath \ "State").readNullable[State].map(Container.apply)
}
