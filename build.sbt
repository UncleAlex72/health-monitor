import com.typesafe.sbt.packager.docker.Cmd
import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

name := """health-monitor"""

lazy val root = (project in file(".")).enablePlugins(PlayScala, DockerPlugin, AshScriptPlugin)

scalaVersion := "2.12.8"

resolvers += Resolver.jcenterRepo

val mongoDbVersion = "1.1.8"

TwirlKeys.templateFormats += ("json" -> "uk.co.unclealex.templates.JsonFormat")

libraryDependencies ++= Seq(
  ehcache,
  ws,
  "uk.co.unclealex" %% "mongodb-scala" % mongoDbVersion,
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "org.typelevel" %% "cats-core" % "1.6.0",
  "org.webjars.npm" % "materialize-css" % "1.0.0-rc.1",
  "org.webjars" % "jquery" % "3.3.1-1",
  "uk.co.unclealex" %% "play-silhouette-mongo" % "1.0.15",
  "uk.co.unclealex" %% "play-browser-push-notifications" % "1.0.0"
)

libraryDependencies ++= Seq(
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2",
  "org.mockito" % "mockito-core" % "2.21.0",
  "uk.co.unclealex" %% "mongodb-scala-test" % mongoDbVersion,
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.13"
).map(_ % Test)

// Docker

dockerBaseImage := "unclealex72/docker-play-healthcheck:latest"
dockerExposedPorts := Seq(9000)
maintainer := "Alex Jones <alex.jones@unclealex.co.uk>"
dockerRepository := Some("unclealex72")
daemonUser in Docker := "root" // required for access to /var/run/docker.sock
dockerUpdateLatest := true
javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)
// Releases

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand("stage"), // : ReleaseStep, build server docker image.
  releaseStepCommand("docker:publish"), // : ReleaseStep, build server docker image.
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)
